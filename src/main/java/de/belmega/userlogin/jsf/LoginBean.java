package de.belmega.userlogin.jsf;

import de.belmega.userlogin.services.AuthService;
import de.belmega.userlogin.services.AuthServiceImpl;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import java.io.Serializable;

/**
 * This class is the Backing Bean for index.xhtml page.
 */
@Named
@SessionScoped
public class LoginBean implements Serializable {


    public static final String INDEX_XHTML_URL = "index.xhtml";
    public static final String USER_IS_LOGGED_IN = "user_is_logged-in";
    public static final String SECRET_XHTML_URL = "secret.xhtml";
    public static final String FACES_REDIRECT_TRUE_PARAMETER = "?faces-redirect=true";

    @Inject
    AuthService authService;
    //The values of the <input> elements on index.xhtml page are bound to these fields:
    private String emailAdress;
    private String password;



    public void setEmailAdress(String emailAdress) {
        this.emailAdress = emailAdress;
    }

    public String getEmailAdress() {
        return emailAdress;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }
    /**
     * Validate the entered user credantials.
     * If the credentials are correct, forward to SECRET_XHTL page
     * Otherwise stay on the current page
     */
    public String validateUsernamePassword() {
        boolean loggedIn = authService.validate(emailAdress, password);
        if (loggedIn) {
            HttpSession session = getHttpSession();
            session.setAttribute(USER_IS_LOGGED_IN, "true");
            return SECRET_XHTML_URL + FACES_REDIRECT_TRUE_PARAMETER;
        } else {
            return " ";
        }
    }

    /**
     *
     * Get current HttpSession object from the FacesContext(JSF framework)
     */
    private HttpSession getHttpSession() {
        return (HttpSession) FacesContext.getCurrentInstance().
                        getExternalContext().getSession(true);
    }

    /**
     *
     * invalidate the user Session and redirect to index page
     */

    public String logout() {
        getHttpSession().invalidate();
        return INDEX_XHTML_URL + FACES_REDIRECT_TRUE_PARAMETER;
    }
}