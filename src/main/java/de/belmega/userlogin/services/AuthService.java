package de.belmega.userlogin.services;

/**
 * Created by majab on 11.09.2017.
 */
public interface AuthService {

    boolean validate (String emailAdress, String passWord);

    byte[] encrypt(char[] passWordPlainText, byte[] salt);

    byte[] generateSalt();
}
