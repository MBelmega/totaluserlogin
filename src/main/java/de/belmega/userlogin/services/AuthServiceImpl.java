package de.belmega.userlogin.services;

        import de.belmega.userlogin.persistence.UserDAO;
        import de.belmega.userlogin.persistence.UserEntity;

        import javax.crypto.SecretKeyFactory;
        import javax.crypto.spec.PBEKeySpec;
        import javax.ejb.Stateless;
        import javax.enterprise.context.ApplicationScoped;
        import javax.inject.Inject;
        import java.security.NoSuchAlgorithmException;
        import java.security.SecureRandom;
        import java.security.spec.InvalidKeySpecException;
        import java.util.Arrays;

/**
 * Created by majab on 06.09.2017.
 */
@Stateless
public class AuthServiceImpl implements AuthService {

    /**
     * Check if th entered password matches the UserEntity from database
     */
    @Inject
    UserDAO dao;

    public boolean validate(String enteredEmailAdress, String enteredPassword) {

        UserEntity userEnt = dao.findUserByMailAdress(enteredEmailAdress);
        if(userEnt == null) return false;
        else{
            boolean passwordIsCorrect = checkPassword(enteredPassword, userEnt);
            return passwordIsCorrect;
        }


    }

    /**
     * Check if entered credentials are valid.
     * @param enteredPasswordPlainText
     * @param userEnt
     * @return true if the entered credentials are valid
     */
    private boolean checkPassword(String enteredPasswordPlainText, UserEntity userEnt) {

        byte[] storedPasswored = userEnt.getEncryptedPassword();

        byte[] enteredPasswordEncrypted = encrypt(enteredPasswordPlainText.toCharArray(),userEnt.getSalt());

        return Arrays.equals(storedPasswored, enteredPasswordEncrypted);
    }
/**
 * Salt the given password and encrypt with PBKDF2WithHmacSHA512-algorithm
 */
    public byte[] encrypt(char[] passWordPlainText, byte[] salt) {

        PBEKeySpec spec = new PBEKeySpec(passWordPlainText, salt, 100, 512);

        try {
            SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
            return skf.generateSecret(spec).getEncoded();
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new RuntimeException(e);
        }


    }

    public byte[] generateSalt() {
        byte[] salt = new byte[64];
        new SecureRandom().nextBytes(salt);
        return salt;
    }
}
