package de.belmega.userlogin.persistence;

import javax.persistence.*;

/**
 * Created by majab on 11.09.2017.
 */
@Entity
public class UserEntity {
    public static final String COLUMN_MAIL_ADRESS = "mailAdress";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name= COLUMN_MAIL_ADRESS, unique = true)
    private String mailAdress;


    private byte[] salt;
    private byte[] encryptedPassword;


    public String getMailAdress() {
        return mailAdress;
    }

    public void setMailAdress(String mailAdress) {
        this.mailAdress = mailAdress;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setSalt(byte[] salt) {
        this.salt = salt;
    }

    public byte[] getSalt() {
        return salt;
    }

    public void setEncryptedPassword(byte[] password) {
        this.encryptedPassword = password;
    }

    public byte[] getEncryptedPassword() {
        return encryptedPassword;
    }
}
