package de.belmega.userlogin.rs;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Creates restapplication for restful webservices
 */
@ApplicationPath("rest")
public class TotalRestApplication extends Application {

}
