package de.belmega.userlogin.filter;

import static de.belmega.userlogin.jsf.LoginBean.*;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;



/**
 * Use the Filter for all requests that contain the.xhtml file extensions.
 */
@WebFilter(filterName = "AuthFilter", urlPatterns = ("*.xhtml"))
public class AuthorizationFilter implements Filter {


    public static final String JAVAX_FACES_RESOURCE = "javax.faces.resource";

    public void init(FilterConfig filterConfig) throws ServletException {
    //do nothing
    }

    /**
     * This method is executed for each HttpRequest that matches the @WebFilter(urlPatterns = {".xhtml})
     * @param request
     * @param response
     * @param chain
     * @throws IOException
     * @throws ServletException
     */

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest httpRequest = (HttpServletRequest) request;
        String url = httpRequest.getRequestURI();

        HttpSession session = httpRequest.getSession(false);

        if (isPrivatePage(url) && !UserIsLoggedIn(session)) {
            redirectToLogin((HttpServletResponse) response);
        } else {
            chain.doFilter(request, response);
        }
    }

    private void redirectToLogin(HttpServletResponse response) throws IOException {
        HttpServletResponse httpResponse = response;
        httpResponse.sendRedirect(INDEX_XHTML_URL);
    }


    /*
    *Check if the HttpSession has the USER_LOGGED_IN
     */
    private boolean UserIsLoggedIn(HttpSession session) {
        boolean sessionHasAttributeUserLoggedIn = session != null && session.getAttribute(USER_IS_LOGGED_IN) != null;
        return sessionHasAttributeUserLoggedIn
                && session.getAttribute(USER_IS_LOGGED_IN).equals("true");
}


    /**
     *
     *Check if the request page is the index page or resource.
     */
    private boolean isPrivatePage(String url) {
        return !(url.contains(INDEX_XHTML_URL) || url.contains(JAVAX_FACES_RESOURCE));
    }


    public void destroy() {
        //do nothing
    }
}
